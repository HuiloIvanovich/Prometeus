#include "EventListener.h"
#include "assert.h"

namespace EngineDll {
	EventListener* EventListener::mEventListener = nullptr;

	EventListener::EventListener() {
		assert(mEventListener == nullptr);
		mEventListener = this;
	}

	EventListener* EventListener::Get() {
		return mEventListener;
	}

	void EventListener::RegisterWatcher(InputWatcher* Watcher) {
		if (Watcher->Inputs.size() > 0)
			mInputWatchersList.push_back(Watcher);
	}


	void EventListener::DeleteWatcher(InputWatcher* Watcher) {
		for (int i = 0; i < mInputWatchersList.size(); i++) {
			if (mInputWatchersList[i] == Watcher)
				mInputWatchersList.erase(mInputWatchersList.begin()+i);
		}
	}

	LRESULT EventListener::Listen(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
		if (msg != WM_DESTROY) {
			if (mInputWatchersList.size() > 0) {
				for (int i = 0; i < mInputWatchersList.size(); i++) {
					for (int j = 0; j < mInputWatchersList[i]->Inputs.size(); j++) {
						if (msg == mInputWatchersList[i]->Inputs[j])
							return mInputWatchersList[i]->Toggle(hwnd, msg, wParam, lParam);
					}
				}
			}
			else 
				return DefWindowProc(hwnd, msg, wParam, lParam);
		}
		else 
			PostQuitMessage(0);
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
}