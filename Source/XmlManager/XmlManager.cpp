#include "XmlManager.h"

namespace EngineDll {
	XmlManager::XmlManager(char* FileName) :filename(FileName) {
		LoadFile();
	}
	bool XmlManager::LoadFile() {					//TODO IMPORTANT: Implement apropriate error handling in case of load failure
		doc.load_file(filename);
		return 1;
	}
	auto XmlManager::GetAttributeValue(char* path) {
		pugi::xpath_node node = doc.select_node(path);
		return node.attribute().value();
	}
}