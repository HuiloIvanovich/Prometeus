#include "Core.h"

namespace EngineDll {
	Engine* Engine::MyEngine = nullptr;

	Engine::Engine(HINSTANCE* hInstance) :mWindowhInstance(hInstance) {
		assert(MyEngine == nullptr);
		MyEngine = this;
		assert(Initialize());
	}

	int Engine::RunMainMessageCycle() {
		MSG msg = { 0 };

		while (msg.message != WM_QUIT)
		{
			// If there are Window messages then process them.
			if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			// Otherwise, do animation/game stuff.
			else
			{
				mpGraphics->Run();			// Actually there will be more complex sequence.
			}
		}
		return (int)msg.wParam;
	}

	bool Engine::Initialize() {
		assert(mpGraphics = new Graphics(*mWindowhInstance));
		assert(mpEngineSettingsXml = new XmlManager("Settings.xml"));
		assert(mpGraphics->Initialize());
		return 1;
	}

	Engine* Engine::Get() {
		return MyEngine;
	}

}
