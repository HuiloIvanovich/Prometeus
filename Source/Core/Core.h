#pragma once
#include "windows.h"

#include "EventListener.h"

#include "Graphics/GraphicsAPI.h"
#include "XmlManager/XmlManager.h"

#ifdef CORE_EXPORTS  
#define CORE_API __declspec(dllexport)   
#else  
#define CORE_API __declspec(dllimport)   
#endif

namespace EngineDll {
	class Engine {

	public:
		CORE_API Engine(HINSTANCE *hInstance);
		CORE_API int RunMainMessageCycle();
		CORE_API static Engine* Get();
	public:
		HINSTANCE *mWindowhInstance = nullptr;

	protected:
		bool Initialize();
	protected:
		Graphics* mpGraphics = nullptr;
		static Engine* MyEngine;
		XmlManager* mpEngineSettingsXml = nullptr;
	};
}
