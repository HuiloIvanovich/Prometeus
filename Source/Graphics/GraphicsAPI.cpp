#include"GraphicsAPI.h"

Graphics* Graphics::mpGraphics = nullptr;

Graphics::Graphics(HINSTANCE hInstance):ShapesApp(hInstance) {
	assert(mpGraphics == nullptr);
	mpGraphics = this;
}

Graphics* Graphics::Get() {
	return mpGraphics;
}

void Graphics::Run() {
	try {
		D3DApp::Run();
	}
	catch (DxException& e) {
		MessageBox(nullptr, e.ToString().c_str(), L"HR Failed", MB_OK);
	}
}

bool Graphics::Initialize() {
	try {
		ShapesApp::Initialize();
	}
	catch (DxException& e) {
		MessageBox(nullptr, e.ToString().c_str(), L"HR Failed", MB_OK);
		return 0;
	}
	return 1;
}