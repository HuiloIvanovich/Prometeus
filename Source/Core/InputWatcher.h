#pragma once
#include <vector>
#include "windows.h"

namespace EngineDll {
	class InputWatcher {
	public:
		std::vector<UINT> Inputs;
		virtual LRESULT Toggle(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
			return DefWindowProc(hwnd, msg, wParam, lParam);
		};
	};
}
