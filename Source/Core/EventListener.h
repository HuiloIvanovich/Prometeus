#pragma once
#include "InputWatcher.h"

namespace EngineDll {
	class EventListener {
	private:
		static EventListener* mEventListener;
	private:
		std::vector<InputWatcher*> mInputWatchersList;
	public:
		EventListener();
		static EventListener* Get();
		LRESULT Listen(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		void RegisterWatcher(InputWatcher* Watcher);
		void DeleteWatcher(InputWatcher* Watcher);
	public:
		
	};
}

