#pragma once
#include "ShapesApp.h"

#ifdef GRAPHICS_EXPORTS  
#define GRAPHICS_API __declspec(dllexport)   
#else  
#define GRAPHICS_API __declspec(dllimport)   
#endif

class Graphics:public ShapesApp {
public:
	GRAPHICS_API Graphics(HINSTANCE hInstance);
	Graphics(const Graphics& rhs) = delete;
	Graphics& operator=(const Graphics& rhs) = delete;
	GRAPHICS_API static Graphics* Get();
	GRAPHICS_API virtual bool Initialize()override;
	GRAPHICS_API virtual void Run()override;
public:

private:
private:
	static Graphics* mpGraphics;
};