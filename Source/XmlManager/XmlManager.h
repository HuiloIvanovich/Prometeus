#pragma once
#include "pugixml.hpp"

#ifdef XMLMANAGER_EXPORTS  
#define XMLMANAGER_API __declspec(dllexport)   
#else  
#define XMLMANAGER_API __declspec(dllimport)   
#endif

namespace EngineDll {
	class XmlManager {
	private:
		bool LoadFile();
	private:
		pugi::xml_document doc;
		char* filename;
	public:
		XMLMANAGER_API XmlManager(char* FileName);
		auto XMLMANAGER_API GetAttributeValue(char* path);
	};
}